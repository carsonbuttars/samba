# Unix SMB/CIFS implementation.
# Copyright Carson Buttars 2024
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from samba.tests import env_get_var_value
import os

from samba.samba3 import libsmb_samba_internal as libsmb
from samba import (ntstatus,NTSTATUSError)
from samba.dcerpc import security as sec
from samba import reparse_symlink
import samba.tests.libsmb

class MountPoints(samba.tests.libsmb.LibsmbTests):

    def setup(self, mountpoint):
        mountpoint_path = mountpoint
        local_dir = env_get_var_value('LOCAL_PATH')
        root_dir = os.path.dirname(local_dir)
        self.fake_mountpoint_conf = os.path.join(root_dir, 'lib', "fake_mountpoint.conf")
        with open(self.fake_mountpoint_conf, 'w+') as f:
            f.write("fake_mountpoint:fstat = %s\n" % (mountpoint_path))

    def connection(self):
        share = "fake_mountpoint"
        smb1 = samba.tests.env_get_var_value("SMB1", allow_missing=True)
        conn = libsmb.Conn(
            self.server_ip,
            share,
            self.lp,
            self.creds,
            force_smb1=smb1)
        return conn

    def clean_file(self, conn, filename):
        try:
            conn.unlink(filename)
        except NTSTATUSError as e:
            err = e.args[0]
            ok = (err == ntstatus.NT_STATUS_OBJECT_NAME_NOT_FOUND)
            ok |= (err == ntstatus.NT_STATUS_OBJECT_PATH_NOT_FOUND)
            ok |= (err == ntstatus.NT_STATUS_IO_REPARSE_TAG_NOT_HANDLED)
            if not ok:
                raise

    def test_mountpoint_detection(self):
        child = 'mount'
        self.setup(child)
        conn = self.connection()
        self.clean_file(conn, child)

        conn.mkdir(child)

        fd,cr,_ = conn.create_ex(child)

        output = conn.list(directory = '')

        mount_dict = [x for x in output if x['name'] == child][0]

        self.assertEqual(
                cr['file_attributes'] & mount_dict['attrib'] & libsmb.FILE_ATTRIBUTE_REPARSE_POINT,
                libsmb.FILE_ATTRIBUTE_REPARSE_POINT)

        conn.close(fd)
        conn.rmdir(child)

    def test_not_incorrect_mountpoint(self):
        child = 'not_mount'
        conn = self.connection()
        self.clean_file(conn, child)

        conn.mkdir(child)

        fd,cr,_ = conn.create_ex(child)

        output = conn.list(directory = '')

        mount_dict = [x for x in output if x['name'] == child][0]

        self.assertNotEqual(
                cr['file_attributes'] & mount_dict['attrib'] & libsmb.FILE_ATTRIBUTE_REPARSE_POINT,
                libsmb.FILE_ATTRIBUTE_REPARSE_POINT)

        conn.close(fd)
        conn.rmdir(child)

