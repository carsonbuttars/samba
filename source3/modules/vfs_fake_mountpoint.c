/*
 *  Unix SMB/CIFS implementation.
 *  Samba VFS module to mock a mountpoint
 *  Copyright (C) Carson Buttars 2024
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#include "includes.h"
#include "smbd/smbd.h"
#include "smbprofile.h"

static int vfs_fake_mountpoint_fstat(vfs_handle_struct *handle, files_struct *fsp,
		SMB_STRUCT_STAT *sbuf)
{
	int result;
	const char * fake_mount_path;

	fake_mount_path = lp_parm_const_string(SNUM(handle->conn),
				       "fake_mountpoint", "fstat", NULL);

	if (strequal(fsp->fsp_name->base_name, fake_mount_path) == 0) {
		START_PROFILE(syscall_fstat);
		result = sys_fstat(fsp_get_pathref_fd(fsp),
				   sbuf, lp_fake_directory_create_times(SNUM(handle->conn)));
		END_PROFILE(syscall_fstat);
		sbuf->st_ex_dev++;

		return result;
	}

	return SMB_VFS_NEXT_FSTAT(handle, fsp, sbuf);
}

static int vfs_fake_mountpoint_openat(vfs_handle_struct *handle,
				      const struct files_struct *dirfsp,
				      const struct smb_filename *smb_fname,
				      files_struct *fsp,
				      const struct vfs_open_how *how)
{
	const char * fake_mount_path;

	fake_mount_path = lp_parm_const_string(SNUM(handle->conn),
				       "fake_mountpoint", "fstat", NULL);

	if (strequal(fsp->fsp_name->base_name, fake_mount_path) == 0) {
		fsp->file_id.devid++;
	}

	return SMB_VFS_NEXT_OPENAT(handle, dirfsp, smb_fname, fsp, how);
}

static struct vfs_fn_pointers vfs_fake_mountpoint_fns = {
	.fstat_fn = vfs_fake_mountpoint_fstat,
	.openat_fn = vfs_fake_mountpoint_openat,
};

static_decl_vfs;
NTSTATUS vfs_fake_mountpoint_init(TALLOC_CTX *ctx)
{
	return smb_register_vfs(SMB_VFS_INTERFACE_VERSION, "fake_mountpoint",
				&vfs_fake_mountpoint_fns);
};
